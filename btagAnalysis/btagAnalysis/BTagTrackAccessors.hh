#ifndef BTAG_TRACK_ACCESSORS_HH
#define BTAG_TRACK_ACCESSORS_HH

#include "AthContainers/AuxElement.h"
// #include "GeoPrimitives/GeoPrimitives.h"

struct BTagTrackAccessors {
  SG::AuxElement::ConstAccessor< float > z0_raw;
  SG::AuxElement::ConstAccessor< float > d0;
  SG::AuxElement::ConstAccessor< float > z0;
  SG::AuxElement::ConstAccessor< float > d0_sigma;
  SG::AuxElement::ConstAccessor< float > z0_sigma;

  SG::AuxElement::ConstAccessor< std::vector<float> > displacement;
  SG::AuxElement::ConstAccessor< std::vector<float> > momentum;

  BTagTrackAccessors():
    z0_raw("btag_z0"),
    d0("btag_ip_d0"),
    z0("btag_ip_z0"),
    d0_sigma("btag_ip_d0_sigma"),
    z0_sigma("btag_ip_z0_sigma"),
    displacement("btag_track_displacement"),
    momentum("btag_track_momentum")
    {
    }

};

#endif
